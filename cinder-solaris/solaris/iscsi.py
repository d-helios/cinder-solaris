from oslo_log import log
from oslo_utils import units
from cinder import exception
from cinder import interface
from cinder.volume import driver
from cinder import ssh_utils
from cinder import utils
import json
import datetime
import math
import random
import sys
from datetime import datetime
from oslo_utils import excutils
from oslo_concurrency import processutils
from cinder.volume.drivers.solaris import utils as zfs_utils
from cinder.volume.drivers.solaris import options as zfs_options
from eventlet import greenthread

LOG = log.getLogger(__name__)


@interface.volumedriver
class ZfsISCSIDriver(driver.ISCSIDriver):
    """Zfs iscsi driver"""

    def create_replica_test_volume(self, volume, src_vref):
        pass

    GENERIC_VOLUME_AND_SNAPSHOT_INFO = """zfs list -t volume,snapshot \
    -H -o name,volsize,origin,tag:cinder_managed,tag:lu,tag:image_id,tag:updated_at -r {base_path}"""

    vendor_name = 'ZFS'
    driver_version = '0.0.1'
    protocol = 'iSCSI'

    # default dataset group name
    default_dg = 'default'

    def __init__(self, *args, **kwargs):
        super(ZfsISCSIDriver, self).__init__(*args, **kwargs)

        self.configuration.append_config_values(zfs_options.ZFS_OPTIONS)

        self.iscsi_target_portal_ip = self.configuration.iscsi_target_portal_ip
        self.iscsi_target_iqn = self.configuration.iscsi_target_iqn
        self.iscsi_volblocksize = self.configuration.iscsi_volblocksize
        self.iscsi_target_group = self.configuration.iscsi_target_group
        self.iscsi_host_group = self.configuration.iscsi_host_group

        self.project_name = self.configuration.project_name
        self.project_compression = self.configuration.project_compression
        self.project_quota = self.configuration.project_quota
        self.project_oversubs_ratio = self.configuration.project_oversubs_ratio
        # preserve clones
        self.project_cache_vols_preserve = self.configuration.project_clone_preserve * 60
        self.project_enable_local_cache = self.configuration.project_enable_local_cache

        self.sshpool = None

    def _run_ssh(self, command, check_exit_code=True, attempts=1):
        """remote command execution runner"""
        start_time = datetime.now()
        exec_frame = sys._getframe(1)

        if not self.sshpool:

            min_size = self.configuration.ssh_min_pool_conn or 1
            max_size = self.configuration.ssh_max_pool_conn or 25

            self.sshpool = ssh_utils.SSHPool(
                self.configuration.ssh_host,
                self.configuration.ssh_port,
                self.configuration.ssh_connection_timeout,
                self.configuration.ssh_user,
                privatekey=self.configuration.ssh_private_key,
                min_size=min_size,
                max_size=max_size)

        last_exception = None
        try:
            with self.sshpool.item() as ssh:
                while attempts > 0:
                    attempts -= 1
                    try:
                        LOG.info('solaris.iscsi._run_ssh. COMMAND: %s' % command)

                        stdout, stderr =  processutils.ssh_execute(
                            ssh,
                            command,
                            check_exit_code=check_exit_code)

                        end_time = datetime.now()
                        LOG.info('solaris.iscsi._run_ssh. FUNCTION: {func_name}, LINE: {line_num}, \
                        TIME_DELTA: {time_delta}.'.format(
                            func_name=exec_frame.f_code.co_name,
                            line_num=exec_frame.f_code.co_firstlineno,
                            time_delta=end_time - start_time,
                        ))

                        return (stdout, stderr)
                    except Exception as e:
                        LOG.error(e)
                        end_time = datetime.now()
                        LOG.info('solaris.iscsi._run_ssh. FUNCTION: {func_name}, LINE: {line_num}, \
TIME_DELTA: {time_delta}.'.format(
                                     func_name=exec_frame.f_code.co_name,
                                     line_num=exec_frame.f_code.co_firstlineno,
                                     time_delta=end_time - start_time,
                                 ))
                        last_exception = e
                        greenthread.sleep(random.randint(20, 500) / 100.0)
                try:
                    raise processutils.ProcessExecutionError(
                        exit_code=last_exception.exit_code,
                        stdout=last_exception.stdout,
                        stderr=last_exception.stderr,
                        cmd=last_exception.cmd)
                except AttributeError:
                    raise processutils.ProcessExecutionError(
                        exit_code=-1,
                        stdout="",
                        stderr="Error running SSH command",
                        cmd=command)
        except Exception:
            with excutils.save_and_reraise_exception():
                LOG.error("Error running SSH command: %s" % command)


    def do_setup(self, context):
        """Do initial setup. Storage has the following structure:
            pool/project/datasetgroup/volume
            We can't create pool within this driver, but we create project, named cinder by default and default
            dataset group, named default.
            We assume that project name must contain poolname, like this tank/cinder
        """

        command = """zfs create -o atime=off -o quota={quota}G -o compression={compression} \
-o mountpoint=none -p {project}/{default_dg}""".format(
            quota=self.project_quota,
            compression=self.project_compression,
            project=self.project_name,
            default_dg=self.default_dg
        )
        self._run_ssh(command)

        # If somebody change quota or compression settings in /etc/cinder/cinder.conf
        # and reload driver, we should apply new settings
        command = """zfs set quota={quota}G {project} && zfs set compression={compression} {project}""".format(
            quota=self.project_quota,
            project=self.project_name,
            compression=self.project_compression
        )
        self._run_ssh(command)

        command = """zfs set quota={quota}G {project}/{default_dg} && zfs set compression={compression} {project}/{default_dg}""".format(
            quota=self.project_quota,
            project=self.project_name,
            compression=self.project_compression,
            default_dg=self.default_dg
        )
        self._run_ssh(command)

    def check_for_setup_error(self):
        """ check for setup error """
        command = 'zfs list -H -o name {project}'.format(project=self.project_name)
        self._run_ssh(command)

    def _update_volume_stats(self):
        """ return common project statistics and count of volumes"""

        # Get project statistics
        command = """zfs list -H -o quota,avail,used {project}""".format(project=self.project_name)
        stdout, stderr = self._run_ssh(command)

        quota, avail, used = map(lambda x: zfs_utils.human2bytes(x), stdout.split())
        LOG.info('iscs._update_volume_stats. SCV good to go, sir. QUOTA: {quota}, AVAIL: {avail}, USED: {used}'.format(
            quota=quota / units.Gi,
            avail=avail / units.Gi,
            used=used / units.Gi
        ))

        # Get vol statistics
        command = 'zfs list -t volume -H -o tag:cinder_managed -r {project}'.format(project=self.project_name)
        stdout, stderr = self._run_ssh(command)

        # Summarize only those volumes whose flag is "on"
        vol_count = len(filter(lambda x: x == 'on', stdout.split()))

        # populate statistic info
        data = {}
        data['vendor_name'] = self.vendor_name
        backend_name = self.configuration.safe_get('volume_backend_name')
        data["volume_backend_name"] = backend_name or self.__class__.__name__
        data['driver_version'] = self.driver_version
        data['storage_protocol'] = 'iscsi'
        data['pools'] = []

        single_pool = {}
        single_pool.update(
            pool_name=self.project_name,
            total_capacity_gb=quota / units.Gi,
            free_capacity_gb=avail / units.Gi,
            provisioned_capacity_gb=used / units.Gi,
            location_info='None',
            QoS_support=False,
            max_over_subscription_ratio=self.project_oversubs_ratio,
            thin_provisioning_support=True,
            thick_provisioning_support=True,
            total_volumes=vol_count,
            multiattach=True,
        )

        data['pools'].append(single_pool)
        self._stats = data

    def create_volume(self, volume):
        """ create volume """

        # create zvol for stmf logical unit
        command = """zfs create -p -V {size}G -s -o tag:cinder_managed=creating \
{project}/{dataset_group}/{volume_name}""".format(
            size=volume['size'],
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

        # create stmf lu
        # root@znstor-v2:/export/home/admin# stmfadm create-lu -p alias=na -p blk=4096 /dev/zvol/rdsk/dpool1/vols/vol1
        # Logical unit created: 600144F0A8038B0000005A50CDF80058
        disk_sn = volume['id'][-12:]
        command = """/opt/stmfha/bin/stmfha create-lu \
-p alias={volume_name} -p blk={blocksize} -p serial={disk_sn} \
/dev/zvol/rdsk/{project}/{dataset_group}/{volume_name}""".format(
            volume_name=volume['name'],
            blocksize=self.iscsi_volblocksize,
            project=self.project_name,
            dataset_group=self.default_dg,
            disk_sn=disk_sn
        )
        stdout, stderr = self._run_ssh(command)

        # get guid of created lu. EX: "Logical unit created: 600144F0A8038B0000005A50CDF80058"
        lu_name = stdout.split()[-1]

        # set additional tag on zvol, to implement reverse search of lu.
        command = """zfs set tag:lu={lu_name} {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

       # set tag to on.
        command = """zfs set tag:cinder_managed=on {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

    def create_volume_from_snapshot(self, volume, snapshot):
        """ create thin volume from snapshot"""

        # create volume from snapshot.
        command = """zfs clone \
-o tag:cinder_managed=creating -o tag:lu=none -o volsize={size}g {project}/{dataset_group}/{volume_name}@{snapshot} \
{project}/{dataset_group}/{clone_name}""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=snapshot['volume_name'],
            snapshot=snapshot['name'],
            clone_name=volume['name'],
            size=volume['size']
        )
        self._run_ssh(command)

        # create logical unit for zvol device
        disk_sn = volume['id'][-12:]
        command = """/opt/stmfha/bin/stmfha create-lu \
-p alias={volume_name} -p blk={blocksize} -p serial={disk_sn} \
/dev/zvol/rdsk/{project}/{dataset_group}/{volume_name}""".format(
            volume_name=volume['name'],
            blocksize=self.iscsi_volblocksize,
            project=self.project_name,
            dataset_group=self.default_dg,
            disk_sn=disk_sn
        )
        stdout, stderr = self._run_ssh(command)

        # get guid of created lu. EX: "Logical unit created: 600144F0A8038B0000005A50CDF80058"
        lu_name = stdout.split()[-1]

        # set additional tag on zvol, to implement reverse search of lu.
        command = """zfs set tag:lu={lu_name} {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

        # set tag to on.
        command = """zfs set tag:cinder_managed=on {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

    def delete_volume(self, volume):
        """ delete volume """

        # checked if volume has snapshots and exit with volume busy if it does
        command = """zfs list -H -t snapshot -r {project}/{dataset_group}/{volume_name}""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        stdout, stderr = self._run_ssh(command)

        if len(stdout) > 0:
            raise exception.VolumeIsBusy(message='Volume has snapshots: %s' % stdout)

        # get lu_name, because we need delete lu first.
        command = """zfs get -H -o value tag:lu {project}/{dataset_group}/{volume_name}""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        stdout, stderr = self._run_ssh(command)

        # delete lu
        lu_name = stdout.strip()
        command = """/opt/stmfha/bin/stmfha delete-lu {lu_name}""".format(
            lu_name=lu_name
        )
        self._run_ssh(command)

        # create job to delete volume
        command = """zfs set tag:cinder_managed=off {project}/{dataset_group}/{volume_name} && zfs set compression=off {project}/{dataset_group}/{volume_name} && nohup bash -c \
"((sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}) ||
(sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}) ||
(sleep $((RANDOM % 60)) && zfs destroy {project}/{dataset_group}/{volume_name}) ||
(sleep $((RANDOM % 90)) && zfs destroy {project}/{dataset_group}/{volume_name}))" < /dev/null > /dev/null 2>&1 &
""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

    def extend_volume(self, volume, new_size):
        """ extend volume"""

        # get lu_name, because we need delete lu first.
        command = """zfs get -H -o value tag:lu {project}/{dataset_group}/{volume_name}""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        stdout, stderr = self._run_ssh(command)
        lu_name = stdout.strip()

        # delete volume with keep-view flag
        command = """/opt/stmfha/bin/stmfha delete-lu -k {lu_name}""".format(
            lu_name=lu_name
        )
        self._run_ssh(command)

        # resize backend
        command = """zfs set volsize={new_size}G {project}/{dataset_group}/{volume_name}""".format(
            new_size=new_size,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

        # create volume with saved guid
        disk_sn = volume['id'][-12:]
        command = """/opt/stmfha/bin/stmfha create-lu \
-p alias={volume_name} -p blk={blocksize} -p guid={lu_name} -p serial={disk_sn} -s {new_size}g \
/dev/zvol/rdsk/{project}/{dataset_group}/{volume_name}""".format(
            volume_name=volume['name'],
            blocksize=self.iscsi_volblocksize,
            lu_name=lu_name,
            new_size=new_size,
            project=self.project_name,
            dataset_group=self.default_dg,
            disk_sn=disk_sn
        )
        self._run_ssh(command)

    def create_snapshot(self, snapshot):
        """ create snapshot """
        command = """zfs snapshot -o tag:cinder_managed=on \
{project}/{dataset_group}/{volume_name}@{snapshot}""".format(
            snapshot=snapshot['name'],
            volume_name=snapshot['volume_name'],
            project=self.project_name,
            dataset_group=self.default_dg
        )
        self._run_ssh(command)

    def delete_snapshot(self, snapshot):
        """ delete snapshot """
        snapshot_name = snapshot['name']
        volume_name = snapshot['volume_name']
        # get clone list and it's origins
        stdout, stderr = self._run_ssh(self.GENERIC_VOLUME_AND_SNAPSHOT_INFO.format(base_path=self.project_name))
        volumes = zfs_utils.parse_volumes(stdout)

        if len(volumes[volume_name]['snapshots'][snapshot_name]['clones']) > 0:
            raise exception.SnapshotIsBusy(message="Snapshot %s is busy. DEBUG: %s" %
                                                   (snapshot_name,
                                                    json.dumps(volumes, sort_keys=True, indent=4,
                                                               separators=(',', ': '))))
        # delete snapshot
        command = """zfs set tag:cinder_managed=off \
{project}/{dataset_group}/{volume_name}@{snapshot} && nohup bash -c \
"((sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}@{snapshot}) || 
(sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}@{snapshot}) ||
(sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}@{snapshot}) ||
(sleep $((RANDOM % 30)) && zfs destroy {project}/{dataset_group}/{volume_name}@{snapshot}))" < /dev/null > /dev/null 2>&1 &
""".format(
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=snapshot['volume_name'],
            snapshot=snapshot['name']
        )
        self._run_ssh(command)

    @utils.trace
    def initialize_connection(self, volume, connector):
        """ export volume to host """
        initiator_iqn = connector['initiator']
        initiator_host = connector['host']

        # check hostgroups
        command = """stmfadm list-hg -v"""
        stdout, stderr = self._run_ssh(command)
        hg_list = zfs_utils.parse_hostgroup(stdout)
        LOG.debug('solaris.iscsi.initialize_connection. List host groups. OUTPUT: %s' %
                  json.dumps(hg_list, sort_keys=True, indent=4, separators=(',', ': ')))

        # check targetgroups
        command = """stmfadm list-tg -v"""
        stdout, stderr = self._run_ssh(command)
        tg_list = zfs_utils.parse_targetgroup(stdout)
        LOG.debug('solaris.iscsi.initialize_connection. List target groups. OUTPUT: %s' %
                  json.dumps(tg_list, sort_keys=True, indent=4, separators=(',', ': ')))

        # create hostgroup if it does not exists
        if initiator_host not in hg_list.keys():
            command = """/opt/stmfha/bin/stmfha create-hg {hostgroup}""".format(hostgroup=initiator_host)
            self._run_ssh(command)

            command = """/opt/stmfha/bin/stmfha add-hg-member -g {hostgroup} {member}""".format(
                hostgroup=initiator_host,
                member=initiator_iqn
            )
            self._run_ssh(command)

        elif initiator_host in hg_list.keys() and initiator_iqn not in hg_list[initiator_host]:
            command = """/opt/stmfha/bin/stmfha add-hg-member -g {hostgroup} {member}""".format(
                hostgroup=initiator_host,
                member=initiator_iqn
            )
            self._run_ssh(command)

        if self.iscsi_target_group not in tg_list.keys():
            command = """/opt/stmfha/bin/stmfha create-tg {targetgroup}""".format(targetgroup=self.iscsi_target_group)
            self._run_ssh(command)

        # check backend
        command = """stmfadm list-lu -v"""
        stdout, stderr = self._run_ssh(command)
        lu_list = zfs_utils.parse_stmflu(stdout)

        # get lu_name corresponding volume['name']
        lu_name = ''
        for key in lu_list.keys():
            if lu_list[key].get('Alias', None) == volume['name']:
                lu_name = key
                break

        if lu_name == '':
            LOG.error('solaris.iscsi.initialize_connection. Logical unit not found for volume %s' % (volume['name']))
            LOG.error('solaris.iscsi.initialize_connection. Logical unit list. OUTPUT: %s' %
                  json.dumps(lu_list, sort_keys=True, indent=4, separators=(',', ': ')))
            raise exception.VolumeNotFound(message='Volume %s not found' % volume['name'])

        # check if lu is not exported, create export
        if lu_list[lu_name]['ViewEntryCount'] == 0:
            command = """/opt/stmfha/bin/stmfha add-view -t {target_group} -h {host_group} {lu_name}""".format(
                target_group=self.iscsi_target_group,
                host_group=initiator_host,
                lu_name=lu_name
            )
            self._run_ssh(command)

        # check if lu already exported to this initiator
        command = """stmfadm list-view -l {lu_name}""".format(lu_name=lu_name)
        stdout, stderr = self._run_ssh(command)
        view_list = zfs_utils.parse_view(stdout)
        LOG.info('solaris.iscsi.initialize_connection. List view. OUTPUT: %s' %
                 json.dumps(view_list, sort_keys=True, indent=4, separators=(',', ': ')))

        for key in view_list.keys():
            if view_list[key]['Hostgroup'] == initiator_host \
                    and view_list[key]['TargetGroup'] == self.iscsi_target_group:
                iscsi_properties = {}
                iscsi_properties.update(
                    target_discovered=False,
                    target_portal=self.iscsi_target_portal_ip,
                    target_iqn=self.iscsi_target_iqn,
                    target_lun=view_list[key]['LUN'],
                    volume_id=lu_list[lu_name]['SerialNum'],
                    discard=True
                )
                LOG.info('Send Volume Info: %s' % str(iscsi_properties))
                return {
                    'driver_volume_type': 'iscsi',
                    'data': iscsi_properties
                }

        # if lun is not exported to inititor. create export
        command = """/opt/stmfha/bin/stmfha add-view -t {target_group} -h {host_group} {lu_name}""".format(
            target_group=self.iscsi_target_group,
            host_group=initiator_host,
            lu_name=lu_name
        )
        self._run_ssh(command)

        command = """stmfadm list-view -l {lu_name}""".format(lu_name=lu_name)
        stdout, stderr = self._run_ssh(command)
        view_list = zfs_utils.parse_view(stdout)
        LOG.debug('solaris.iscsi.initialize_connection. List views to find created view. OUTPUT: %s' %
                  json.dumps(view_list, sort_keys=True, indent=4, separators=(',', ': ')))

        # find created view
        for key_2 in view_list.keys():
            if view_list[key_2]['Hostgroup'] == initiator_host \
                    and view_list[key_2]['TargetGroup'] == self.iscsi_target_group:
                iscsi_properties = {}
                iscsi_properties.update(
                    target_discovered=False,
                    target_portal=self.iscsi_target_portal_ip,
                    target_iqn=self.iscsi_target_iqn,
                    target_lun=view_list[key_2]['LUN'],
                    volume_id=lu_list[lu_name]['SerialNum'],
                    discard=True
                )
                LOG.info('Send Volume Info: %s' % str(iscsi_properties))
                return {
                    'driver_volume_type': 'iscsi',
                    'data': iscsi_properties
                }

    @utils.trace
    def terminate_connection(self, volume, connector, **kwargs):
        """ unexport volume """
        initiator_host = connector['host']

        # get lu_name for specified volume['name']
        command = """stmfadm list-lu -v"""
        stdout, stderr = self._run_ssh(command)
        lu_list = zfs_utils.parse_stmflu(stdout)
        LOG.debug('solaris.iscsi.terminate_connection. Get logical unit list. OUTPUT: %s' % lu_list)

        lu_name = ''
        view_entry_count = 0
        for key in lu_list.keys():
            if lu_list[key].get('Alias', None) == volume['name']:
                lu_name = key
                view_entry_count = lu_list[key].get('ViewEntryCount', 0)
                break

        if lu_name == '':
            LOG.error('solaris.iscsi.initialize_connection. Logical unit not found for volume %s' % (volume['name']))
            LOG.error('solaris.iscsi.initialize_connection. Logical unit list. OUTPUT: %s' %
                  json.dumps(lu_list, sort_keys=True, indent=4, separators=(',', ': ')))
            raise exception.VolumeNotFound(message='Volume %s not found' % volume['name'])

        if view_entry_count == 0:
            LOG.debug('solaris.iscsi.initialize_connection. Logical unit not exported at all. VOL: %s, ViewEntryCount: %s' % (lu_name, view_entry_count))
            return 
            
        # get export list
        command = """stmfadm list-view -l {lu_name}""".format(lu_name=lu_name)
        stdout, stderr = self._run_ssh(command)
        view_list = zfs_utils.parse_view(stdout)
        LOG.debug('solaris.iscsi.terminate_connection. List view. OUTPUT: %s' %
                  json.dumps(view_list, sort_keys=True, indent=4, separators=(',', ': ')))

        for key in view_list.keys():
            if view_list[key]['Hostgroup'] == initiator_host \
                    and view_list[key]['TargetGroup'] == self.iscsi_target_group:
                command = """/opt/stmfha/bin/stmfha remove-view -l {lu_name} {view_entry}""".format(
                    lu_name=lu_name,
                    view_entry=key
                )
                self._run_ssh(command)

    @utils.synchronized('solaris', external=True)
    def clone_image(self, context, volume, image_location, image_meta, image_service):
        """Create a volume efficiently from an existing image.

        Verify the image ID being used:

        (1) If there is no existing cache volume, create one and transfer
        image data to it. Take a snapshot.

        (2) If a cache volume already exists, take a clone and update field named updated_at.

        A file lock is placed on this method to prevent:

        (a) a race condition when a cache volume has been verified, but then
        gets deleted before it is cloned.

        (b) failure of subsequent clone_image requests if the first request is
        still pending.
        """

        LOG.debug('Cloning image %(image)s to volume %(volume)s',
                  {'image': image_meta['id'], 'volume': volume['name']})

        if not self.project_enable_local_cache:
            return None, False

        cachevol_size = image_meta['size']
        if 'virtual_size' in image_meta and image_meta['virtual_size']:
            cachevol_size = image_meta['virtual_size']

        # get volume size in gigabytes
        cachevol_size_gb = int(math.ceil(float(cachevol_size) / units.Gi))

        # Make sure the volume is big enough since cloning adds extra metadata.
        # Having it as X Gi can cause creation failures.
        if cachevol_size % units.Gi == 0:
            cachevol_size_gb += 1

        # not enough space to place image to volume
        if cachevol_size_gb > volume['size']:
            exception_msg = ('Image size %(img_size)dGB is larger '
                             'than volume size %(vol_size)dGB.',
                             {'img_size': cachevol_size_gb,
                              'vol_size': volume['size']})
            LOG.error(exception_msg)
            return None, False

        cachevol_props = {'size': cachevol_size_gb}

        # get volume name and snapshot name
        cache_vol, cache_snap = self._verify_cache_volume(context,
                                                          image_meta,
                                                          image_service,
                                                          cachevol_props)

        if cache_vol == cache_snap is None:
            return None, False

        snapshot_meta = {
            'name': cache_snap,
            'volume_name': cache_vol,
            'size': cachevol_size_gb
        }

        self.create_volume_from_snapshot(volume, snapshot_meta)

        # remove old volumes
        self._remove_old_volumes(snapshot_meta)

        return None, True

    def _remove_old_volumes(self, snapshot):

        updated_at = datetime.datetime.now()
        command = """zfs set tag:updated_at={updated_at} {project}/{dataset_group}/{volume}""".format(
            updated_at=updated_at.strftime('%Y%m%d%H%M%S'),
            project=self.project_name,
            dataset_group=self.default_dg,
            volume=snapshot['volume_name']
        )
        self._run_ssh(command)

        # find cache volumes without clones and remove old.
        stdout, stderr = self._run_ssh(self.GENERIC_VOLUME_AND_SNAPSHOT_INFO.format(base_path=self.project_name))
        vols = zfs_utils.parse_volumes(stdout)

        for_remove = []
        for vol in vols.keys():
            if 'cache-' in vol:
                if len(vols[vol]['snapshots']) > 0:
                    snap_name = vols[vol]['snapshots'].keys()[0]
                    vol_updated_at = datetime.datetime.strptime(vols[vol]['tag_updated_at'], '%Y%m%d%H%M%S')
                    time_delta = updated_at - vol_updated_at
                    if len(vols[vol]['snapshots'][snap_name]['clones']) == 0 \
                            and time_delta.total_seconds() > self.project_cache_vols_preserve:
                        for_remove.append({"volume_name": vol, "snapshot_name": snap_name})

        for vol in for_remove:
            snapshot_meta = {
                'volume_name': vol['volume_name'],
                'name': vol['snapshot_name']
            }
            self.delete_snapshot(snapshot_meta)

            volume_meta = {
                'name': vol['volume_name']
            }
            self.delete_volume(volume_meta)

    def _verify_cache_volume(self, context, img_meta, img_service, cachevol_props):
        """Verify if we have a cache volume that we want.
        If we don't, create one.
        If we do, check if it's been updated:
          * If so, delete it and recreate a new volume
          * If not, we are good.

        If it's out of date, delete it and create a new one.
        After the function returns, there should be a cache volume available,
        ready for cloning.
        """
        updated_at = datetime.datetime.now().strftime('%Y-%M-%d')

        cachevol_name = 'cache-%s' % img_meta['id']
        cachesnap_name = 'image-%s' % img_meta['id']
        cachevol_meta = {
            'cache_name': cachevol_name,
            'snap_name': cachesnap_name,
        }

        cachevol_props.update(cachevol_meta)

        LOG.debug('Verifying cache volume: %s', cachevol_name)
        stdout, stderr = self._run_ssh(self.GENERIC_VOLUME_AND_SNAPSHOT_INFO.format(base_path=self.project_name))
        vols = zfs_utils.parse_volumes(stdout)

        if cachevol_name in vols.keys() and cachesnap_name in vols[cachevol_name]['snapshots']:
            return cachevol_name, cachesnap_name
        else:
            self._create_cache_volume(context, img_meta, img_service, cachevol_props)

            # update volume information
            commad = """zfs set tag:updated_at={updated_at} \
{project}/{dataset_group}/{volume} && zfs set tag:image_id={image_id} {project}/{dataset_group}/{volume}""".format(
                updated_at=updated_at,
                project=self.project_name,
                dataset_group=self.default_dg,
                volume=cachevol_name,
                image_id=img_meta['id'],
            )
            self._run_ssh(commad)

            return cachevol_name, cachesnap_name

    def _create_cache_volume(self, context, img_meta,
                             img_service, cachevol_props):
        """Create a cache volume from an image.
                    Returns names of the cache volume and its snapshot.
        """
        cachevol_size = int(cachevol_props['size'])
        cache_vol = {
            'name': cachevol_props['cache_name'],
            'id': img_meta['id'],
            'size': cachevol_size,
        }
        LOG.debug('Creating cache volume %s.', cache_vol['name'])
        try:
            self.create_volume(cache_vol)
        except Exception as e:
            self.delete_volume(cache_vol)
            raise processutils.ProcessExecutionError(stdout="""Can't create cache volume.""", stderr=str(e))

        super(ZfsISCSIDriver, self).copy_image_to_volume(context,
                                                         cache_vol,
                                                         img_service,
                                                         img_meta['id'])
        cache_snap = {
            'name': cachevol_props['snap_name'],
            'volume_name': cachevol_props['cache_name'],
            'size': cachevol_size,
        }
        self.create_snapshot(cache_snap)

    def local_path(self, volume):
        """Not implemented."""
        pass

    def create_export(self, context, volume, connector):
        pass

    def remove_export(self, context, volume):
        pass

    def ensure_export(self, context, volume):
        pass

    def create_cloned_volume(self, volume, src_vref):
        # clone volume from snapshot.

        clone_snapshot = {'volume_name': src_vref['name'],
                          'name': 'tmp-snapshot-%s' % volume['id']}

        self.create_snapshot(clone_snapshot)
        try:
            command = """zfs send \
{project}/{dataset_group}/{volume}@{snapshot} | zfs recv -o tag:cinder_managed=creating \
{project}/{dataset_group}/{clone}""".format(
                project=self.project_name,
                dataset_group=self.default_dg,
                volume=clone_snapshot['volume_name'],
                snapshot=clone_snapshot['name'],
                clone=volume['name']
            )
            self._run_ssh(command)
        except Exception as e:
            self.delete_snapshot(clone_snapshot)
            raise exception.VolumeBackendAPIException(message=e.message)

        self.delete_snapshot(clone_snapshot)

        # create logical unit for zvol device
        disk_sn = volume['id'][-12:]
        command = """/opt/stmfha/bin/stmfha create-lu \
-p alias={volume_name} -p blk={blocksize} -p serial={disk_sn} \
/dev/zvol/rdsk/{project}/{dataset_group}/{volume_name}""".format(
            volume_name=volume['name'],
            blocksize=self.iscsi_volblocksize,
            project=self.project_name,
            dataset_group=self.default_dg,
            disk_sn=disk_sn
        )
        stdout, stderr = self._run_ssh(command)

        # get guid of created lu. EX: "Logical unit created: 600144F0A8038B0000005A50CDF80058"
        lu_name = stdout.split()[-1]

        # set additional tag on zvol, to implement reverse search of lu.
        command = """zfs set tag:lu={lu_name} {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

        # set cinder_managed=on.
        command = """zfs set tag:cinder_managed=on {project}/{dataset_group}/{volume_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name']
        )
        self._run_ssh(command)

        # set cinder_managed=on for temporary snapshot
        # it's needed because parse_volumes function process only managed objects
        command = """zfs set tag:cinder_managed=on {project}/{dataset_group}/{volume_name}@{snapshot_name}""".format(
            lu_name=lu_name,
            project=self.project_name,
            dataset_group=self.default_dg,
            volume_name=volume['name'],
            snapshot_name=clone_snapshot['name']
        )
        self._run_ssh(command)

        # remove snapshot on cloned volume
        clone_snapshot.update(volume_name=volume['name'])
        self.delete_snapshot(clone_snapshot)


