from oslo_config import cfg

ZFS_OPTIONS = [
    # SSH Section
    cfg.StrOpt('ssh_host',
               default='',
               help='Ip address of solaris server'),
    cfg.IntOpt('ssh_port',
               default=22,
               help='Default connection port'),
    cfg.StrOpt('ssh_private_key',
               default='',
               help='Default connection port'),
    cfg.IntOpt('ssh_connection_timeout',
               default=120,
               help='Default connection timeout'),
    cfg.StrOpt('ssh_user',
               default='root',
               help='User name to connect to solaris server'),
    cfg.IntOpt('ssh_min_pool_conn',
               default=1,
               help='Initial size of ssh connection pool to solaris server'),
    cfg.IntOpt('ssh_max_pool_conn',
               default=25,
               help='Max size of ssh connection pool to solaris server'),

    # ISCSI Section
    cfg.StrOpt('iscsi_target_portal_ip',
               default='',
               help='Target portal address'),
    cfg.StrOpt('iscsi_target_iqn',
               default='iqn.2018-01.ru.peter-service:01:cinder-c8107a5070a0',
               help='Target IQN'),
    cfg.IntOpt('iscsi_volblocksize',
               default=4096,
               help='Default volblock size for iscsi volumes'),
    cfg.StrOpt('iscsi_host_group',
               default='cinder',
               help='Host group to place initiator IQN'),
    cfg.StrOpt('iscsi_target_group',
               default='cinder',
               help='Target group to place target IQN'),

    # Project settings
    cfg.StrOpt('project_name',
               default='tank/cinder',
               help='Root dataset for cinder volumes'),
    cfg.IntOpt('project_quota',
               default=8,
               help='Quota for cinder volumes'),
    cfg.StrOpt('project_compression',
               default='on',
               help='Compression algorithm'),
    cfg.IntOpt('project_oversubs_ratio',
               default=1,
               help='Project over subscription'),
    cfg.IntOpt('project_clone_preserve',
               default=12,
               help='How many days store empty cache volumes'),
    cfg.BoolOpt('project_enable_local_cache', default=True,
                help='Flag to enable local caching: True, False.'),

]

CONF = cfg.CONF
CONF.register_opts(ZFS_OPTIONS)

