SYMBOLS = {
    'customary': ('B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'),
    'customary_ext': ('byte', 'kilo', 'mega', 'giga', 'tera', 'peta', 'exa',
                      'zetta', 'iotta'),
    'iec': ('Bi', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'),
    'iec_ext': ('byte', 'kibi', 'mebi', 'gibi', 'tebi', 'pebi', 'exbi',
                'zebi', 'yobi'),
}


def bytes2human(n, format='%(value).1f %(symbol)s', symbols='customary'):
    """
    Convert n bytes into a human readable string based on format.
    symbols can be either "customary", "customary_ext", "iec" or "iec_ext",
    see: http://goo.gl/kTQMs

      >>> bytes2human(0)
      '0.0 B'
      >>> bytes2human(0.9)
      '0.0 B'
      >>> bytes2human(1)
      '1.0 B'
      >>> bytes2human(1.9)
      '1.0 B'
      >>> bytes2human(1024)
      '1.0 K'
      >>> bytes2human(1048576)
      '1.0 M'
      >>> bytes2human(1099511627776127398123789121)
      '909.5 Y'

      >>> bytes2human(9856, symbols="customary")
      '9.6 K'
      >>> bytes2human(9856, symbols="customary_ext")
      '9.6 kilo'
      >>> bytes2human(9856, symbols="iec")
      '9.6 Ki'
      >>> bytes2human(9856, symbols="iec_ext")
      '9.6 kibi'

      >>> bytes2human(10000, "%(value).1f %(symbol)s/sec")
      '9.8 K/sec'

      >>> # precision can be adjusted by playing with %f operator
      >>> bytes2human(10000, format="%(value).5f %(symbol)s")
      '9.76562 K'
    """
    n = int(n)
    if n < 0:
        raise ValueError("n < 0")
    symbols = SYMBOLS[symbols]
    prefix = {}
    for i, s in enumerate(symbols[1:]):
        prefix[s] = 1 << (i + 1) * 10
    for symbol in reversed(symbols[1:]):
        if n >= prefix[symbol]:
            value = float(n) / prefix[symbol]
            return format % locals()
    return format % dict(symbol=symbols[0], value=n)


def human2bytes(s):
    """
    Attempts to guess the string format based on default symbols
    set and return the corresponding bytes as an integer.
    When unable to recognize the format ValueError is raised.

      >>> human2bytes('0 B')
      0
      >>> human2bytes('1 K')
      1024
      >>> human2bytes('1 M')
      1048576
      >>> human2bytes('1 Gi')
      1073741824
      >>> human2bytes('1 tera')
      1099511627776

      >>> human2bytes('0.5kilo')
      512
      >>> human2bytes('0.1  byte')
      0
      >>> human2bytes('1 k')  # k is an alias for K
      1024
      >>> human2bytes('12 foo')
      Traceback (most recent call last):
          ...
      ValueError: can't interpret '12 foo'
    """
    init = s
    num = ""

    if s == '0':
        return 0

    while s and s[0:1].isdigit() or s[0:1] == '.':
        num += s[0]
        s = s[1:]
    num = float(num)
    letter = s.strip()
    for name, sset in SYMBOLS.items():
        if letter in sset:
            break
    else:
        if letter == 'k':
            # treat 'k' as an alias for 'K' as per: http://goo.gl/kTQMs
            sset = SYMBOLS['customary']
            letter = letter.upper()
        else:
            raise ValueError("can't interpret %r" % init)
    prefix = {sset[0]: 1}
    for i, s in enumerate(sset[1:]):
        prefix[s] = 1 << (i + 1) * 10
    return int(num * prefix[letter])

def parse_stmflu(output):
    numeric_fieds = ['BlockSize', 'Size', 'ViewEntryCount']
    logical_units = {}
    for lu_output in output.split('\n')[:-1]:
        key, value = lu_output.replace(' ', '').split(':')
        if key == 'LUName':
            lu_name = value
            logical_units[lu_name] = {}
            continue
        logical_units[lu_name][key]= int(value) if key in numeric_fieds else value
    return logical_units


def parse_view(output):
    numeric_fieds = ['ViewEntry', 'LUN']
    views = {}
    for view_output in output.split('\n')[:-1]:
        key, value = view_output.replace(' ', '').split(':')
        if key == 'ViewEntry':
            view_entry = value
            views[view_entry] = {}
            continue
        views[view_entry][key] = int(value) if key in numeric_fieds else value
    return views


def parse_hostgroup(output):
    hostgroups = {}
    for hg_output in output.split('\n')[:-1]:
        rows = hg_output.replace(' ', '').split(':')
        if rows[0] == 'HostGroup':
            hg_entry = rows[1]
            hostgroups[hg_entry] = []
            continue
        hostgroups[hg_entry].append(":".join(rows[1:]))

    return hostgroups


def parse_targetgroup(output):
    targetgroups = {}
    for hg_output in output.split('\n')[:-1]:
        rows = hg_output.replace(' ', '').split(':')
        if rows[0] == 'TargetGroup':
            tg_entry = rows[1]
            targetgroups[tg_entry] = []
            continue
        targetgroups[tg_entry].append(":".join(rows[1:]))

    return targetgroups


def parse_origin(output):
    origins = {}
    for line in output.split('\n')[:-1]:
        volume, origin = line.split()
        if origins.has_key(origin):
            origins[origin].append(volume)
        else:
            origins[origin] = []
            origins[origin].append(volume)
    return origins


def parse_volumes(output):
    volumes = {}
    for line in output.split('\n')[:-1]:
        name, volsize, origin, tag_cinder_managed, tag_lu, tag_image_id, tag_updated_at = line.split()
        volume, snapshot, origin_volume, origin_snapshot = None, None, None, None

        if tag_cinder_managed != 'on':
            continue

        volsize_bytes = None
        if volsize != '-':
            volsize_bytes = human2bytes(volsize)

        if '@' in name:
            volume, snapshot = map(lambda x: x.split('/')[-1], name.split('@'))
        else:
            volume = name.split('/')[-1]

        if '@' in origin:
            origin_volume, origin_snapshot = origin.split('@')

        # create volume if not exist
        if volume not in volumes.keys():
            volumes[volume] = {}
            volumes[volume]['tag_cinder_managed'] = tag_cinder_managed
            volumes[volume]['tag_lu'] = tag_lu
            volumes[volume]['tag_image_id'] = tag_image_id
            volumes[volume]['tag_updated_at'] = tag_updated_at
            volumes[volume]['snapshots'] = {}
            volumes[volume]['origin'] = {}

        if snapshot:
            volumes[volume]['snapshots'][snapshot] = {}
            volumes[volume]['snapshots'][snapshot]['tag_cinder_managed'] = tag_cinder_managed
            volumes[volume]['snapshots'][snapshot]['tag_image_id'] = tag_image_id
            volumes[volume]['snapshots'][snapshot]['tag_updated_at'] = tag_updated_at
            volumes[volume]['snapshots'][snapshot]['clones'] = []

        if not snapshot:
            volumes[volume]['size'] = volsize_bytes

        if not snapshot and origin != '-':
            origin_volume, origin_snapshot = map(lambda x: x.split('/')[-1], origin.split('@'))
            volumes[volume]['origin'].update(
                volume=origin_volume,
                snapshot=origin_snapshot
            )

    for volume in volumes.keys():
        if len(volumes[volume]['origin']) > 0:
            origin_volume = volumes[volume]['origin']['volume']
            origin_snapshot = volumes[volume]['origin']['snapshot']
            volumes[origin_volume]['snapshots'][origin_snapshot]['clones'].append(volume)

    return volumes