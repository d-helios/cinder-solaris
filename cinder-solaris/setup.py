#!/usr/bin/env python

import os
import sys

from distutils.core import setup
import subprocess

name = "cinder-solaris"
version = subprocess.check_output(['git', 'tag']).split('\n')[-2]

rootdir = os.path.abspath(os.path.dirname(__file__))
# Restructured text project description read from file
long_description = open(os.path.join(rootdir, 'README.md')).read()
setup(name='python-' + name,
      version=version,
      description='cinder driver for Oracle Solaris COMSTAR',
      author='Artem Penner',
      author_email='apenner.it@gmail.com',
      license='Apache-2',
      keywords='openstack cinder solaris',
      packages=['solaris'],
      )
