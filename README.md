# Examples
## Create volume

Try to create cinder volume
```
openstack client:~ # os volume create --size 10 --type zfs zvol1
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| consistencygroup_id | None                                 |
| created_at          | 2018-02-26T18:37:11.453001           |
| description         | None                                 |
| encrypted           | False                                |
| id                  | ce9ed712-f6ac-4d38-b1dc-0d41c510c208 |
| migration_status    | None                                 |
| multiattach         | False                                |
| name                | zvol1                                |
| properties          |                                      |
| replication_status  | disabled                             |
| size                | 10                                   |
| snapshot_id         | None                                 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | zfs                                  |
| updated_at          | None                                 |
| user_id             | f7a98c28cffa4be492d6308c1ee55024     |
+---------------------+--------------------------------------+
```

Let's see it on solaris side
```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5
NAME                                                                       TAG:CINDER_MANAGED  USED  VOLSIZE
dpool5/cinder-testing/default/volume-ce9ed712-f6ac-4d38-b1dc-0d41c510c208  on                  165K      10G

```

As you can see volume id and zfs dataset name has a identical name.

## Create volume snapshot

```
openstack client:~ # os snapshot create --name snap1 zvol1
+-------------+--------------------------------------+
| Field       | Value                                |
+-------------+--------------------------------------+
| created_at  | 2018-02-26T18:41:54.321476           |
| description | None                                 |
| id          | fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff |
| name        | snap1                                |
| properties  |                                      |
| size        | 10                                   |
| status      | creating                             |
| updated_at  | None                                 |
| volume_id   | ce9ed712-f6ac-4d38-b1dc-0d41c510c208 |
+-------------+--------------------------------------+
```

```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5
NAME                                                                                                                     TAG:CINDER_MANAGED  USED  VOLSIZE
dpool5/cinder-testing/default/volume-ce9ed712-f6ac-4d38-b1dc-0d41c510c208                                                on                  165K      10G
dpool5/cinder-testing/default/volume-ce9ed712-f6ac-4d38-b1dc-0d41c510c208@snapshot-fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff  on                     0        -
```

## Create volume from snapshot
```
openstack client:~ # os volume create --snapshot fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff --size 15 zclone1
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| consistencygroup_id | None                                 |
| created_at          | 2018-02-26T18:48:07.207598           |
| description         | None                                 |
| encrypted           | False                                |
| id                  | 4c1eead5-f241-42df-9284-e5b9753711d6 |
| migration_status    | None                                 |
| multiattach         | False                                |
| name                | zclone1                              |
| properties          |                                      |
| replication_status  | disabled                             |
| size                | 15                                   |
| snapshot_id         | fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff |
| source_volid        | None                                 |
| status              | creating                             |
| type                | zfs                                  |
| updated_at          | None                                 |
| user_id             | f7a98c28cffa4be492d6308c1ee55024     |
+---------------------+--------------------------------------+
```

```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5
NAME                                                                                                                     TAG:CINDER_MANAGED  USED  VOLSIZE
dpool5/cinder-testing/default/volume-4c1eead5-f241-42df-9284-e5b9753711d6                                                on                  165K      15G
dpool5/cinder-testing/default/volume-ce9ed712-f6ac-4d38-b1dc-0d41c510c208                                                on                  165K      10G
dpool5/cinder-testing/default/volume-ce9ed712-f6ac-4d38-b1dc-0d41c510c208@snapshot-fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff  on                     0        -
```

Now, let's try to delete snapshot, on which volume is based (expected, that we recieve an a error - VolumeBusy)
```
eopenstack client:~ # os snapshot list
+--------------------------------------+-------+-------------+-----------+------+
| ID                                   | Name  | Description | Status    | Size |
+--------------------------------------+-------+-------------+-----------+------+
| fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff | snap1 | None        | available |   10 |
+--------------------------------------+-------+-------------+-----------+------+
openstack client:~ # os snapshot delete fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff
openstack client:~ # os snapshot list
+--------------------------------------+-------+-------------+-----------+------+
| ID                                   | Name  | Description | Status    | Size |
+--------------------------------------+-------+-------------+-----------+------+
| fcc18b80-3e43-4b7e-b4dd-ba94a7ff82ff | snap1 | None        | available |   10 |
+--------------------------------------+-------+-------------+-----------+------+
```

Now, try to delete base volume
```
openstack client:~ # os volume delete zvol1
Failed to delete volume with name or ID 'zvol1': Invalid volume: Volume status must be available or error or error_restoring or error_extending and must not be migrating, attached, belong to a group or have snapshots. (HTTP 400) (Request-ID: req-9605b706-e50c-45de-9074-33c2e58a659f)
1 of 1 volumes failed to delete.
openstack client:~ #
openstack client:~ # os volume show zvol1
+--------------------------------+---------------------------------------------------------+
| Field                          | Value                                                   |
+--------------------------------+---------------------------------------------------------+
| attachments                    | []                                                      |
| availability_zone              | nova                                                    |
| bootable                       | false                                                   |
| consistencygroup_id            | None                                                    |
| created_at                     | 2018-02-26T18:37:11.453001                              |
| description                    | None                                                    |
| encrypted                      | False                                                   |
| id                             | ce9ed712-f6ac-4d38-b1dc-0d41c510c208                    |
| migration_status               | None                                                    |
| multiattach                    | False                                                   |
| name                           | zvol1                                                   |
| os-vol-host-attr:host          | srv-os-ctlst03.net.billing.ru@zfs#dpool5/cinder-testing |
| os-vol-mig-status-attr:migstat | None                                                    |
| os-vol-mig-status-attr:name_id | None                                                    |
| os-vol-tenant-attr:tenant_id   | 372dbd7bd56546e58b13e992c58b31ac                        |
| properties                     |                                                         |
| replication_status             | disabled                                                |
| size                           | 10                                                      |
| snapshot_id                    | None                                                    |
| source_volid                   | None                                                    |
| status                         | available                                               |
| type                           | zfs                                                     |
| updated_at                     | 2018-02-26T18:37:12.842001                              |
| user_id                        | f7a98c28cffa4be492d6308c1ee55024                        |
+--------------------------------+---------------------------------------------------------+
openstack client:~ #
```

## Destroy clone, snapshot and volume

```
openstack client:~ # os volume delete zclone1
openstack client:~ # os snapshot delete snap1
openstack client:~ # os volume delete zvol1
```

```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5

no datasets available
```

## Clone image to volume

```
openstack client:~ # os image list
+--------------------------------------+--------------------------------+--------+
| ID                                   | Name                           | Status |
+--------------------------------------+--------------------------------+--------+
| 91f2fe5f-0ef4-455d-bcef-077e1bd954d4 | centos7                        | active |
| 40f6a0a0-7553-4c3e-b51b-d0aa8ddfb175 | Red Hat Enterprise Linux 7 ZFS | active |
| a8aa57e5-e16e-49ed-b821-970df43c072f | cirros                         | active |
| 4d605196-1cfa-427a-8306-5c36a0ad43d5 | Red Hat Enterprise Linux 7     | active |
| 490c241e-9035-4d42-81d4-3440a5b0b704 | rhel7.4                        | active |
+--------------------------------------+--------------------------------+--------+

openstack client:/var/lib/cinder # os volume create --image 91f2fe5f-0ef4-455d-bcef-077e1bd954d4 --type zfs --size 10 boot_vol1
+---------------------+--------------------------------------+
| Field               | Value                                |
+---------------------+--------------------------------------+
| attachments         | []                                   |
| availability_zone   | nova                                 |
| bootable            | false                                |
| consistencygroup_id | None                                 |
| created_at          | 2018-02-26T19:07:56.019837           |
| description         | None                                 |
| encrypted           | False                                |
| id                  | c3c1a55d-67a1-4c2c-9858-c00d7860e140 |
| migration_status    | None                                 |
| multiattach         | False                                |
| name                | boot_vol1                            |
| properties          |                                      |
| replication_status  | disabled                             |
| size                | 10                                   |
| snapshot_id         | None                                 |
| source_volid        | None                                 |
| status              | creating                             |
| type                | zfs                                  |
| updated_at          | None                                 |
| user_id             | f7a98c28cffa4be492d6308c1ee55024     |
+---------------------+--------------------------------------+
```

```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5
NAME                                                                                                                 TAG:CINDER_MANAGED   USED  VOLSIZE
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4                                             on                  1.75G       9G
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4@image-91f2fe5f-0ef4-455d-bcef-077e1bd954d4  on                      0        -
dpool5/cinder-testing/default/volume-c3c1a55d-67a1-4c2c-9858-c00d7860e140                                            on                   165K      10G
```

Let's try to create another volume from image and you see that's it created from a snapshot

```
zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5                                                                                                                           Mon Feb 26 22:13:13 2018

NAME                                                                                                                 TAG:CINDER_MANAGED   USED  VOLSIZE
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4                                             on                  1.75G       9G
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4@image-91f2fe5f-0ef4-455d-bcef-077e1bd954d4  on                      0        -
dpool5/cinder-testing/default/volume-c3c1a55d-67a1-4c2c-9858-c00d7860e140
```


## Create VM on cinder volume
```
openstack client:/var/lib/cinder # os server create --key-name=apenner --nic=net-id=79b008d0-8b23-4bb6-b188-b3d0b8b46d8d --volume boot_vol1 --flavor 4e4c266e-90be-4c34-98cd-aa2b36383f52 vm1

openstack client:/var/lib/cinder # os server list
+--------------------------------------+---------+--------+------------------------+----------------------------+
| ID                                   | Name    | Status | Networks               | Image Name                 |
+--------------------------------------+---------+--------+------------------------+----------------------------+
| deee60c7-13e8-459a-b5e1-16767917f57c | vm1     | ACTIVE | PROVIDER=172.30.216.43 |                            |
| 569ce54b-ef05-46e7-a366-dffd02b66d78 | myvm    | ACTIVE | PROVIDER=172.30.216.52 |                            |
| 5cdaca43-c14f-4c29-9ab0-65a875ffccc3 | test    | ACTIVE | PROVIDER=172.30.216.50 |                            |
| 52f4d48e-beb5-4671-8a83-dd801658b07c | iowandr | ACTIVE | PROVIDER=172.30.216.11 | Red Hat Enterprise Linux 7 |
+--------------------------------------+---------+--------+------------------------+----------------------------+

openstack client:/var/lib/cinder # os volume list
+--------------------------------------+------------------+----------------+------+-------------------------------+
| ID                                   | Display Name     | Status         | Size | Attached to                   |
+--------------------------------------+------------------+----------------+------+-------------------------------+
| c3c1a55d-67a1-4c2c-9858-c00d7860e140 | boot_vol1        | in-use         |   10 | Attached to vm1 on /dev/vda   |
+--------------------------------------+------------------+----------------+------+-------------------------------+
```

# Terraform example

```
[root@srv-cinder-dev ~]#  os volume create --image 91f2fe5f-0ef4-455d-bcef-077e1bd954d4 --type zfs --size 10 boot_vol_centos
[root@srv-cinder-dev ~]# os snapshot create --name snapshot1 boot_vol_centos
```

```
resource "openstack_compute_instance_v2" "basic" {
  count = 10
  name            = "basic-${count.index}"
  flavor_id       = "f3194f8e-c9a6-416b-8f41-f6f48c062b79"
  key_pair        = "apenner"
  security_groups = ["default"]

  network {
    name = "PROVIDER"
  }

  block_device {
    uuid                  = "5cb4d626-4b99-4f8c-9793-e2323c46632a"
    source_type           = "snapshot"
    volume_size           = 15
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }
}
```

```
openstack client:~/example # os server list
+--------------------------------------+---------+--------+------------------------+----------------------------+
| ID                                   | Name    | Status | Networks               | Image Name                 |
+--------------------------------------+---------+--------+------------------------+----------------------------+
| cdcfda14-113c-4ea3-af3e-25cf9d0fba5f | basic-5 | ACTIVE | PROVIDER=172.30.216.65 |                            |
| b6373842-01c5-4b0f-b733-14e219d94107 | basic-9 | ACTIVE | PROVIDER=172.30.216.21 |                            |
| 87e7e9a2-e3fa-492b-9282-aab264dd88cb | basic-8 | ACTIVE | PROVIDER=172.30.216.58 |                            |
| a463da24-7c6a-45c0-a839-2ae45425a5b0 | basic-3 | ACTIVE | PROVIDER=172.30.216.59 |                            |
| f814c3a5-f089-477f-847e-ec3b44f0576b | basic-4 | ACTIVE | PROVIDER=172.30.216.51 |                            |
| 6cf230e8-08d8-4741-a427-f5f9ac165ae6 | basic-2 | ACTIVE | PROVIDER=172.30.216.39 |                            |
| 63f17f22-35b9-4bed-9129-fe478f17d4a2 | basic-7 | ACTIVE | PROVIDER=172.30.216.40 |                            |
| 18e9f2db-542d-4fb6-bfff-ca4d1c8b56e2 | basic-0 | ACTIVE | PROVIDER=172.30.216.18 |                            |
| 1f3a2ad6-d65f-430a-b6c4-6daeed61d048 | basic-6 | ACTIVE | PROVIDER=172.30.216.42 |                            |
| da8efec7-4066-4d32-a6a5-7f1adb535ae7 | basic-1 | ACTIVE | PROVIDER=172.30.216.54 |                            |
+--------------------------------------+---------+--------+------------------------+----------------------------+
```

```
Every 3.0s: zfs list -t volume,snapshot -o name,tag:cinder_managed,used,volsize -r dpool5
NAME                                                                                                                     TAG:CINDER_MANAGED   USED  VOLSIZE
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4                                                 on                  1.75G       9G
dpool5/cinder-testing/default/cache-91f2fe5f-0ef4-455d-bcef-077e1bd954d4@image-91f2fe5f-0ef4-455d-bcef-077e1bd954d4      on                      0        -
dpool5/cinder-testing/default/volume-0ae483d8-b7e5-4b05-8a95-334535f87382                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-13d0525c-ecd9-421e-aa63-f190b7ac342b                                                on                   165K      10G
dpool5/cinder-testing/default/volume-13d0525c-ecd9-421e-aa63-f190b7ac342b@snapshot-5cb4d626-4b99-4f8c-9793-e2323c46632a  on                      0        -
dpool5/cinder-testing/default/volume-29c5b584-0243-4387-a8bc-eca5c233b8ee                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-2bafc759-5cd1-4706-916f-5397c5245806                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-42939d7d-2e59-4bc8-a1b9-305aca04e8db                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-4ed6db0a-c4ba-49ff-b0d8-a9097317a68c                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-6291a30b-6302-4a46-9bff-99cb44731920                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-78a54c2b-fcf8-495b-8907-5d2f788e6ce5                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-7ac9558d-3441-46a3-9cdb-31b3d5c99c0c                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-dc2c0551-148b-4e99-8fef-10cec9ba2d27                                                on                  11.7M      15G
dpool5/cinder-testing/default/volume-ee4af46d-8fa3-4b69-8c5e-f558d1333f0a                                                on                  11.7M      15G
```

