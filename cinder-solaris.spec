%define version %{lua:print(os.getenv("VERSION"))}
%define buildnum %{lua:print(os.getenv("BUILD_NUMBER"))}
%define cinder_sitelib %(%{__python} -c 'import os; import cinder; print os.path.dirname(cinder.__file__)')
%define _unpackaged_files_terminate_build 0



Summary:        Cinder driver for Oracle Solaris COMSTAR.
Name:           cinder-solaris
Version:        %{version}
Release:        %{buildnum}%{?dist}
License:        Apache-2.0
Group:          System/Management
Source:         cinder-solaris.%{version}.tar.gz
Requires:       python-cinder
Requires:       openstack-cinder
BuildRequires:  python-cinder
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildArch:      noarch

%description
Cinder driver for Oracle Solaris COMSTAR.

%prep
%setup -n cinder-solaris-%{version}

%build
%{__python} setup.py build


%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root="%{buildroot}" --prefix="%{_prefix}" --install-lib="%{cinder_sitelib}"/volume/drivers

%clean
%{__rm} -rf %{buildroot}

%files
%{cinder_sitelib}/volume/drivers/solaris
