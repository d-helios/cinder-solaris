# cinder-solaris
all: build clean

dependencies:
	yum install -y rpm-build git python-cinder
	mkdir -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

build: dependencies
	$(eval DRIVER_VERSION:= `git describe --tags --abbrev=0`)
	@echo "building version ${DRIVER_VERSION}"
	mkdir -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
	cp -r cinder-solaris cinder-solaris-${DRIVER_VERSION}
	tar -czvf rpmbuild/SOURCES/cinder-solaris.${DRIVER_VERSION}.tar.gz cinder-solaris-${DRIVER_VERSION}
	cp cinder-solaris.spec rpmbuild/SPECS
	VERSION=${DRIVER_VERSION} rpmbuild -vv -bb rpmbuild/SPECS/cinder-solaris.spec  --define "_topdir %(pwd)/rpmbuild"
	cp rpmbuild/RPMS/noarch/*rpm .

clean:
	rm -rf rpmbuild
	rm -rf cinder-solaris-${DRIVER_VERSION}

.PHONY: dependencies build clean
